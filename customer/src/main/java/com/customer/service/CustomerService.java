package com.customer.service;

import com.customer.dto.CustomerRegistrationRequest;
import com.customer.model.Customer;
import com.customer.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.amqp.RabbitMQMessageProducer;
import org.clients.fraud.FraudCheckResponse;
import org.clients.fraud.FraudClient;
import org.clients.notification.NotificationRequest;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {


    private final CustomerRepository customerRepository;

    private final FraudClient fraudClient;

    private final RabbitMQMessageProducer producer;


    public void registerCustomer(CustomerRegistrationRequest request) {
        Customer customer = Customer.builder()
                .firstName(request.firstName())
                .lastName(request.lastName())
                .email(request.email())
                .build();

        customerRepository.saveAndFlush(customer);

        FraudCheckResponse response = fraudClient.isFraud(customer.getId());

        if (response.isFraudster()) {
            throw new IllegalStateException("fraudster");
        }

        NotificationRequest notificationRequest = new NotificationRequest(
                customer.getId(),
                customer.getEmail(),
                String.format("hello %s , this is your notif body", customer.getFirstName())
        );

        producer.publish(notificationRequest,
                "internal.exchange",
                "internal.notification.routing-key");
    }
}
